#include "tobiigazecore\include\tobiigaze.h"
#include "tobiigazecore\include\tobiigaze_discovery.h"
#include <Windows.h>

extern "C"
{
    #include "include\lua.h"                           
    #include "include\lauxlib.h"                           
    #include "include\lualib.h"                            
}

class Mutex
{
public:
	Mutex() : _hMutex(0) 
	{
		_hMutex = CreateMutex(0, false, 0);
	}

	virtual ~Mutex()
	{
		CloseHandle(_hMutex);
	}

	bool TakeMutex() const
	{
		DWORD dwWaitResult = WaitForSingleObject(_hMutex, INFINITE);
		if (dwWaitResult == WAIT_OBJECT_0)
			return true;

		return false;
	}

	void ReleaseMutex() const
	{
		::ReleaseMutex(_hMutex);
	}

private:
	HANDLE _hMutex;
};

class ScopedLock
{
public:
	ScopedLock(const Mutex& mutex) : _mutex(mutex)
	{
		_mutex.TakeMutex();
	}

	virtual ~ScopedLock()
	{
		_mutex.ReleaseMutex();
	}

private:
	const Mutex& _mutex;
};

static Mutex mutex;



//#define DEBUG_PRINTS

#ifdef DEBUG_PRINTS
#define LOG printf("calling %s\n", __FUNCDNAME__);
#else
#define LOG ;
#endif

#define LUA_EYETRACKER_INARG(pos) \
    luaL_checktype(L, pos, LUA_TLIGHTUSERDATA); \
    tobiigaze_eye_tracker* eye_tracker = static_cast< tobiigaze_eye_tracker*>(lua_touserdata(L, 1));

#define LUA_PUSH_ERROR \
    lua_pushnumber(L, ec);

static int tobiigaze_create_lua(lua_State *L)
{
    LOG
    luaL_checktype(L, 1, LUA_TSTRING);
    const char* url = lua_tostring(L, 1);
    tobiigaze_error_code ec;
    tobiigaze_eye_tracker* eye_tracker = tobiigaze_create(url, &ec);	

    lua_pushlightuserdata(L, eye_tracker);
	LUA_PUSH_ERROR
    return 2;
}

static int tobiigaze_run_event_loop_on_internal_thread_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)
    tobiigaze_run_event_loop_on_internal_thread(eye_tracker, nullptr, nullptr);
    return 1;
}

static int tobiigaze_connect_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)
    tobiigaze_error_code ec;
    tobiigaze_connect(eye_tracker, &ec);
    LUA_PUSH_ERROR
    return 1;   
}

struct lua_callback_context
{
    lua_State* lua_state;
    int func_ref;
};

static lua_callback_context gazedata_context;

static void internal_gazedata_callback(const tobiigaze_gaze_data* gd, const tobiigaze_gaze_data_extensions* gdext, void* user_data)
{
    lua_rawgeti(gazedata_context.lua_state, LUA_REGISTRYINDEX, gazedata_context.func_ref);

    lua_newtable(gazedata_context.lua_state);
    lua_newtable(gazedata_context.lua_state);
    lua_pushnumber(gazedata_context.lua_state, gd->left.gaze_point_on_display_normalized.x);
    lua_setfield(gazedata_context.lua_state, -2, "x");
    lua_pushnumber(gazedata_context.lua_state, gd->left.gaze_point_on_display_normalized.y);
    lua_setfield(gazedata_context.lua_state, -2, "y");
    lua_setfield(gazedata_context.lua_state, -2, "left");

    lua_newtable(gazedata_context.lua_state);
    lua_pushnumber(gazedata_context.lua_state, gd->right.gaze_point_on_display_normalized.x);
    lua_setfield(gazedata_context.lua_state, -2, "x");
    lua_pushnumber(gazedata_context.lua_state, gd->right.gaze_point_on_display_normalized.y);
    lua_setfield(gazedata_context.lua_state, -2, "y");
    lua_setfield(gazedata_context.lua_state, -2, "right");
    
    lua_pushnumber(gazedata_context.lua_state, (lua_Number)gd->timestamp);
    lua_setfield(gazedata_context.lua_state, -2, "timestamp");

    lua_pushnumber(gazedata_context.lua_state, gd->tracking_status);
    lua_setfield(gazedata_context.lua_state, -2, "tracking_status");

    lua_pcall(gazedata_context.lua_state, 1, 0, 0);
}

static int tobiigaze_start_tracking_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)
    luaL_checktype(L, 2, LUA_TFUNCTION);
    int func_ref = luaL_ref(L, LUA_REGISTRYINDEX);
    
    gazedata_context.lua_state = L;
    gazedata_context.func_ref = func_ref;
    
    tobiigaze_error_code ec;
    tobiigaze_start_tracking(eye_tracker, internal_gazedata_callback, &ec, nullptr);
    LUA_PUSH_ERROR
    return 1;   
}

static const tobiigaze_gaze_data* last_gaze_data = 0;

static void internal_gazedata_callback_no_callback(const tobiigaze_gaze_data* gd, const tobiigaze_gaze_data_extensions* gdext, void* user_data)
{
	ScopedLock lock(mutex);	
	last_gaze_data = gd;
}

static int tobiigaze_start_tracking_internal_lua(lua_State *L)
{
	LOG
    LUA_EYETRACKER_INARG(1)
	tobiigaze_error_code ec;
    tobiigaze_start_tracking(eye_tracker, internal_gazedata_callback_no_callback, &ec, nullptr);
	LUA_PUSH_ERROR
    return 1;   
}

static int tobiigaze_get_last_gaze_data_lua(lua_State *L)
{
	ScopedLock lock(mutex);
	if (!last_gaze_data)
	{
		return 0;
	}

	lua_newtable(L);
    lua_newtable(L);
    lua_pushnumber(L, last_gaze_data->left.gaze_point_on_display_normalized.x);
    lua_setfield(L, -2, "x");
    lua_pushnumber(L, last_gaze_data->left.gaze_point_on_display_normalized.y);
    lua_setfield(L, -2, "y");
    lua_setfield(L, -2, "left");

    lua_newtable(L);
    lua_pushnumber(L, last_gaze_data->right.gaze_point_on_display_normalized.x);
    lua_setfield(L, -2, "x");
    lua_pushnumber(L, last_gaze_data->right.gaze_point_on_display_normalized.y);
    lua_setfield(L, -2, "y");
    lua_setfield(L, -2, "right");
    
    lua_pushnumber(L, (lua_Number)last_gaze_data->timestamp);
    lua_setfield(L, -2, "timestamp");

    lua_pushnumber(L, last_gaze_data->tracking_status);
    lua_setfield(L, -2, "tracking_status");
	return 1;
}

static int tobiigaze_break_event_loop_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)
    tobiigaze_break_event_loop(eye_tracker);
    return 1;
}

static int tobiigaze_disconnect_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)
    tobiigaze_disconnect(eye_tracker);
    return 1;
}

static int tobiigaze_stop_tracking_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)
    tobiigaze_error_code ec;
    tobiigaze_stop_tracking(eye_tracker, &ec);
    LUA_PUSH_ERROR
    return 1;
}

static int tobiigaze_get_device_info_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)

    tobiigaze_error_code ec;
    tobiigaze_device_info info;
    tobiigaze_get_device_info(eye_tracker, &info, &ec);

    if (!ec)
    {
        lua_newtable(L);
        lua_pushstring(L, info.model);
        lua_setfield(L, -2, "model");
        lua_pushstring(L, info.generation);
        lua_setfield(L, -2, "generation");
        lua_pushstring(L, info.firmware_version);
        lua_setfield(L, -2, "firmware_version");
        lua_pushstring(L, info.serial_number);
        lua_setfield(L, -2, "serial_number");
    }

    LUA_PUSH_ERROR
    return 2;
}

static int tobiigaze_destroy_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)
    
    tobiigaze_destroy(eye_tracker);

    return 1;
}

static lua_callback_context error_callback_context;

static void internal_error_callback(tobiigaze_error_code error_code, void* user_data)
{
    lua_rawgeti(error_callback_context.lua_state, LUA_REGISTRYINDEX, error_callback_context.func_ref);
    lua_pushnumber(error_callback_context.lua_state, error_code); 
    lua_pcall(error_callback_context.lua_state, 1, 0, 0);
}


static int tobiigaze_register_error_callback_lua(lua_State *L)
{
    LOG
    LUA_EYETRACKER_INARG(1)

    luaL_checktype(L, 2, LUA_TFUNCTION);
    int func_ref = luaL_ref(L, LUA_REGISTRYINDEX);
    
    error_callback_context.lua_state = L;
    error_callback_context.func_ref = func_ref;
    
    tobiigaze_register_error_callback(eye_tracker, internal_error_callback , nullptr);
    
    return 1;
}

static int tobiigaze_get_connected_eye_tracker_lua(lua_State *L)
{
    LOG;
    tobiigaze_error_code ec;
    char url[128];
    tobiigaze_get_connected_eye_tracker(url, 128, &ec);
    lua_pushstring(L, url);
    LUA_PUSH_ERROR
    return 2;   
}

static int tobiigaze_get_screen_resolution_lua(lua_State *L)
{
    LOG;
    int x = GetSystemMetrics(SM_CXSCREEN);
    int y = GetSystemMetrics(SM_CYSCREEN);
    lua_pushnumber(L, x);
    lua_pushnumber(L, y);
    return 2;
}


extern "C" __declspec(dllexport) int luaopen_tobiigazecore_lua(lua_State *L){

    lua_register(L, "tobiigaze_create", tobiigaze_create_lua);
	lua_register(L, "tobiigaze_connect", tobiigaze_connect_lua);
    lua_register(L, "tobiigaze_run_event_loop_on_internal_thread", tobiigaze_run_event_loop_on_internal_thread_lua);
    lua_register(L, "tobiigaze_start_tracking", tobiigaze_start_tracking_lua);
	lua_register(L, "tobiigaze_start_tracking_internal", tobiigaze_start_tracking_internal_lua);
	lua_register(L, "tobiigaze_get_last_gaze_data", tobiigaze_get_last_gaze_data_lua);	
    lua_register(L, "tobiigaze_disconnect", tobiigaze_disconnect_lua);
    lua_register(L, "tobiigaze_break_event_loop", tobiigaze_break_event_loop_lua);
    lua_register(L, "tobiigaze_stop_tracking", tobiigaze_stop_tracking_lua);
    lua_register(L, "tobiigaze_get_device_info", tobiigaze_get_device_info_lua);
    lua_register(L, "tobiigaze_destroy", tobiigaze_destroy_lua);
    lua_register(L, "tobiigaze_register_error_callback", tobiigaze_register_error_callback_lua);
    lua_register(L, "tobiigaze_get_connected_eye_tracker", tobiigaze_get_connected_eye_tracker_lua);
    lua_register(L, "tobiigaze_get_screen_resolution", tobiigaze_get_screen_resolution_lua);
    return 0;
}