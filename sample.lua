require("tobiigazecore_lua")

tracker_url, err = tobiigaze_get_connected_eye_tracker()
if (err ~= 0) then
    print("No tracker found")
    os.exit()
end

print("Connecting to ", tracker_url)
tracker = tobiigaze_create(tracker_url)
tobiigaze_run_event_loop_on_internal_thread(tracker)
err = tobiigaze_connect(tracker)
if err ~= 0 then
    print("Failed to connect: ",err) 
    os.exit()
end

devinfo, err = tobiigaze_get_device_info(tracker)
print("Model: ", devinfo.model)

x, y = tobiigaze_get_screen_resolution()
print("Screen resolution: ", x, y)

tobiigaze_start_tracking(tracker, function (gazedata)
        print("timestamp: ", gazedata.timestamp)
        print("tracking status: ", gazedata.tracking_status)
        print("left: ", gazedata.left.x, gazedata.left.y)
        print("right ", gazedata.right.x, gazedata.right.y)
    end)
    
    
os.execute("sleep 1")

tobiigaze_stop_tracking(tracker)
print("Stopping")

tobiigaze_disconnect(tracker)
tobiigaze_destroy(tracker)