------------ Lua bindings for Tobii GazeSDK ------------

Provides basic Lua bindings for Tobii's GazeSDK version 4.0.2. Check out the sample.lua for example usage. So far only Windows support.

Compiles with Visual Studio 2012. Remember to set LUA_PATH to where your Lua lib/include folder is. Build tested with Lua 5.2.3. You can get prebuilt binaries at http://sourceforge.net/projects/luabinaries/files/5.2.3/. Copy gazesdk dlls to same folder as tobiigaze_lua.dll.

See the Downloads section for a prebuilt version of the Lua bindings.

For all eye tracking related topics and for GazeSDK support, please visit http://developer.tobii.com/.